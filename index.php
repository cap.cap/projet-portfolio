<?php

    $dsn = 'pgsql:host=127.0.0.1;dbname=portfolio';
    $user = 'postgres';
    $password = 'PostGre1379';

    try {
        $dbh = new PDO($dsn, $user, $password);
    } catch (PDOException $e) {
        echo 'Connexion échouée : ' .$e->getMessage();
    }

$receptionAccueil = $dbh->query("SELECT * FROM admin.accueil");
$receptionFormations = $dbh->query("SELECT * FROM admin.formations");
$receptionProjets = $dbh->query("SELECT * FROM admin.projets");
$receptionDiplomes = $dbh->query("SELECT * FROM admin.diplomes");
$receptionExperiences = $dbh->query("SELECT * FROM admin.experiences");
$receptionLoisirs = $dbh->query("SELECT * FROM admin.loisirs");
$receptionContact = $dbh->query("SELECT * FROM admin.contact");


if (isset($_POST['submit']) && !empty($_POST['nom']) && !empty($_POST['mail']) && !empty($_POST['message'])) {
    $nom = $_POST['nom'];
    $mail = $_POST['mail'];
    $message = $_POST['message'];

    $sth = $dbh->prepare('INSERT INTO admin.formulaire(formulaire_nom,formulaire_mail,formulaire_message) VALUES (:nom, :mail, :message)');

    $sth->bindValue('nom', $nom);
    $sth->bindValue('mail', $mail);
    $sth->bindValue('message', $message);

    $sth->execute();
}

?>

<!DOCTYPE html>
<html>
	<head>
		<link rel="icon" type="img/jpg" href="moi.png">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link rel="stylesheet" type="text/css" href="portfoliocss.css">
		<link rel="shortcut icon" type="image/png" href="moi.png">
		<title>Capucine Bechemin</title>
	</head>
	<body>
		<nav role="navigation">
			<div id="menuToggle">

			    <input type="checkbox"/>

			    <span></span>
			    <span></span>
			    <span></span>

                <ul id="menu">
                    <a href="#pres"><li>Accueil</li></a>
                    <a href="#formations"><li>Formations</li></a>
                    <a href="#projets"><li>Projets</li></a>
                    <a href="#diplomes"><li>Diplômes</li></a>
                    <a href="#experiencesPro"><li>Expériences Professionnelles</li></a>
                    <a href="#loisirs"><li>Loisirs</li></a>
                    <a href="#contact"><li>Contact</li></a>
                    <a href="administration/administration.php?page=accueil"><li>Administration</li></a>
		        </ul>
		    </div>
        </nav>

<div id="pres">
    <?php
        while ($row = $receptionAccueil->fetch(PDO::FETCH_ASSOC)):
    ?>
	<h1><?= $row['accueil_titre']?></h1>
    <?php
        endwhile;
    ?>
	<a href="#formations"><button>BONNE VISITE ET BELLE JOURNEE A VOUS</button></a>
</div>

<div id="formations">
		<h2>Formations</h2>
		<div>
            <?php
                while ($row = $receptionFormations->fetch(PDO::FETCH_ASSOC)):
            ?>
                <p><?= $row['formations_texte']?></p>
            <?php
                endwhile;
            ?>
        </div>
		<div>
			<img id="languages" src="formations/cssjs.png">
		</div>
</div>

<div id="projets">
		<h2>Projets</h2>
		<div>
            <?php
                while ($row = $receptionProjets->fetch(PDO::FETCH_ASSOC)):
            ?>
			<h3><?= $row['projets_titre']?></h3>
            <h3> Du <?= $row['projets_date_debut']?> au <?= $row['projets_date_fin']?></h3>
                <p><?= $row['projets_texte']?></p>
            <?php
                endwhile;
            ?>
		</div>

</div>
<div id="ensavoirplus">
	<div id="diplomesexp">
		<div id="diplomes">
			<h2>Diplômes</h2>
			<div id="photo">
				<div>
                    <?php
                        while ($row = $receptionDiplomes->fetch(PDO::FETCH_ASSOC)):
                    ?>
					<img id="diplImg" src="<?= $row['diplomes_photo']?>" height="120" title="<?= $row['diplomes_texte']?>">
                    <?php
                        endwhile;
                    ?>
				</div>
			</div>
		</div>

		<div id="experiencesPro">
			<h2>Experiences</h2>
			<div>
                <?php
                    while ($row = $receptionExperiences->fetch(PDO::FETCH_ASSOC)):
                ?>
                <h3><?= $row['experiences_entreprise']?></h3>
                <h3>Du <?= $row['experiences_date_debut']?> au <?= $row['experiences_date_fin']?></h3>
				<p><?= $row['experiences_texte']?></p>
                <?php
                    endwhile;
                ?>
			</div>
		</div>
	</div>
<a href="#loisirs"><button>EN SAVOIR PLUS SUR MOI</button></a>
</div>



<div id="loisirs">
	<h2>Loisirs</h2>
	<div>
        <?php
            while ($row = $receptionLoisirs->fetch(PDO::FETCH_ASSOC)):
        ?>
		<h3><?= $row['loisirs_titre']?></h3>
			<p><?= $row['loisirs_texte']?></p>
        <?php
            endwhile;
        ?>
	</div>
	
		<a href="#contact"><button>ME CONTACTER</button></a>
</div>



<div id="contact">
	<form method="POST" action="portfolio.php">
		<h2>Contactez-moi</h2>
		<div>
			<label id="nomdf" for="nom"></label>
			<br>
			<input id="a" type="text" name="nom" placeholder="Votre nom" required>
		</div>
		<div>
			<label id="mail" for="mail"></label>
			<br>
			<input id="c" type="email" name="mail" placeholder="Votre adresse mail" required>
		</div>
		<div>
			<label id="message" for="message"></label>
			<br>
			<textarea name="message" id="d" placeholder="Vos questions ou vos demandes.." required></textarea>
		</div>
		
		<input type="submit" name="submit" value="ENVOYER">
	</form>



	<div id="fbin">
        <?php
            while ($row = $receptionContact->fetch(PDO::FETCH_ASSOC)):
        ?>
		<a href="<?= $row['contact_fb']?>"><img src="contact/fb.png" height="50"></a>
		<a href="<?= $row['contact_insta']?>"><img src="contact/instagram.png" height="50"></a>
        <?php
            endwhile;
        ?>
	</div>
</div>
        <script type="text/javascript" src="portfolio.js"></script>
	</body>
</html>