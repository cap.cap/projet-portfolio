CADRE DU PROJET 

    Réalisation et mise en place de votre site web/CV. 
    Nous utiliserons les compétences suivantes : 
    
        TECHNOLOGIES WEB HTML & CSS 
        JAVASCRIPT 
        ERGONOMIE 
        BASE DE DONNEES MERISE
        SQL 
    
    Il est nécessaire d’avoir les prérequis suivants : 
    
    • Savoir configurer un hébergement web
    • GIT 
       

DESCRIPTIF DU PROJET 

   La création d’un site web CV. 
   Le portfolio comporte les pages suivantes : 
    
    • Présentation simple : nom, prénom, photo, compétences clefs 
    • Présentation détaillée: expériences professionnelles, compétences, rubriques libres, Réalisations/Projets 
    • Page de contact permettant aux visiteurs de vous contacter par le biais d’un formulaire 
    
    Ainsi qu'une section d’administration permettant de:
    
    • Gérer votre page de présentation simple (ajout de photo, édition des textes en WYSIWYG) 
    • Gérer votre page de présentation détaillée : 
            Gestion des expériences professionnelles (date de début/date de fin, titre, nom de l’organisme/entreprise, détails, etc.) 
            Gestion de votre parcours scolaire (diplômes, écoles, dates de scolarité, etc.) 
            Gestion de vos compétences clefs (par exemple : HTML (3/5), CSS (5/5), Javascript (2/5)
            Gestion des rubriques libres (sports, loisirs, centres d’intérêts, etc.) 
    • Gérer votre page Réalisations/Projets : 
            Gestion des réalisations/projets : screenshots, technos utilisés, liens vers un dépôt GIT, etc. 
    • Gérer la page de contact 
            Pouvoir changer les textes.
            Les messages envoyés doivent être pouvoir être stockés dans une base de données et consultés dans votre interface d’administration.

INFOS UTILES

    • instaler GIT puis cloner le projet GitLab 
    (https://www.geeksforgeeks.org/setup-gitlab-repository-windows-10/)
    • instaler le serveur web permetant d'executer PHP en local (Wamp)
    • instaler PostgreSQL, creer une database et y entrer les commande du fichier SQL du git
