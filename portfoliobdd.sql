--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3
-- Dumped by pg_dump version 11.3

-- Started on 2019-06-06 11:03:55

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 6 (class 2615 OID 24685)
-- Name: admin; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA admin;


ALTER SCHEMA admin OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 202 (class 1259 OID 24743)
-- Name: accueil; Type: TABLE; Schema: admin; Owner: postgres
--

CREATE TABLE admin.accueil (
    accueil_titre character varying(50),
    id integer NOT NULL
);


ALTER TABLE admin.accueil OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 32964)
-- Name: accueil_id_seq; Type: SEQUENCE; Schema: admin; Owner: postgres
--

CREATE SEQUENCE admin.accueil_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.accueil_id_seq OWNER TO postgres;

--
-- TOC entry 2900 (class 0 OID 0)
-- Dependencies: 205
-- Name: accueil_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: postgres
--

ALTER SEQUENCE admin.accueil_id_seq OWNED BY admin.accueil.id;


--
-- TOC entry 201 (class 1259 OID 24707)
-- Name: contact; Type: TABLE; Schema: admin; Owner: postgres
--

CREATE TABLE admin.contact (
    contact_fb character varying(200),
    contact_insta character varying(200),
    id integer NOT NULL
);


ALTER TABLE admin.contact OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 32973)
-- Name: contact_id_seq; Type: SEQUENCE; Schema: admin; Owner: postgres
--

CREATE SEQUENCE admin.contact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.contact_id_seq OWNER TO postgres;

--
-- TOC entry 2901 (class 0 OID 0)
-- Dependencies: 206
-- Name: contact_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: postgres
--

ALTER SEQUENCE admin.contact_id_seq OWNED BY admin.contact.id;


--
-- TOC entry 198 (class 1259 OID 24695)
-- Name: diplomes; Type: TABLE; Schema: admin; Owner: postgres
--

CREATE TABLE admin.diplomes (
    diplomes_photo character varying(200),
    diplomes_texte character varying(200),
    id integer NOT NULL
);


ALTER TABLE admin.diplomes OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 32985)
-- Name: diplomes_id_seq; Type: SEQUENCE; Schema: admin; Owner: postgres
--

CREATE SEQUENCE admin.diplomes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.diplomes_id_seq OWNER TO postgres;

--
-- TOC entry 2902 (class 0 OID 0)
-- Dependencies: 207
-- Name: diplomes_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: postgres
--

ALTER SEQUENCE admin.diplomes_id_seq OWNED BY admin.diplomes.id;


--
-- TOC entry 200 (class 1259 OID 24704)
-- Name: experiences; Type: TABLE; Schema: admin; Owner: postgres
--

CREATE TABLE admin.experiences (
    experiences_texte character varying(200),
    id integer NOT NULL,
    experiences_entreprise character varying(100),
    experiences_date_debut date,
    experiences_date_fin date
);


ALTER TABLE admin.experiences OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 32997)
-- Name: experiences_id_seq; Type: SEQUENCE; Schema: admin; Owner: postgres
--

CREATE SEQUENCE admin.experiences_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.experiences_id_seq OWNER TO postgres;

--
-- TOC entry 2903 (class 0 OID 0)
-- Dependencies: 208
-- Name: experiences_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: postgres
--

ALTER SEQUENCE admin.experiences_id_seq OWNED BY admin.experiences.id;


--
-- TOC entry 197 (class 1259 OID 24692)
-- Name: formations; Type: TABLE; Schema: admin; Owner: postgres
--

CREATE TABLE admin.formations (
    formations_texte character varying(200),
    id integer NOT NULL
);


ALTER TABLE admin.formations OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 33009)
-- Name: formations_id_seq; Type: SEQUENCE; Schema: admin; Owner: postgres
--

CREATE SEQUENCE admin.formations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.formations_id_seq OWNER TO postgres;

--
-- TOC entry 2904 (class 0 OID 0)
-- Dependencies: 209
-- Name: formations_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: postgres
--

ALTER SEQUENCE admin.formations_id_seq OWNED BY admin.formations.id;


--
-- TOC entry 204 (class 1259 OID 32946)
-- Name: formulaire; Type: TABLE; Schema: admin; Owner: postgres
--

CREATE TABLE admin.formulaire (
    id integer NOT NULL,
    formulaire_nom character varying(25),
    formulaire_mail character varying(60),
    formulaire_message character varying(500)
);


ALTER TABLE admin.formulaire OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 32944)
-- Name: formulaire_id_seq; Type: SEQUENCE; Schema: admin; Owner: postgres
--

CREATE SEQUENCE admin.formulaire_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.formulaire_id_seq OWNER TO postgres;

--
-- TOC entry 2905 (class 0 OID 0)
-- Dependencies: 203
-- Name: formulaire_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: postgres
--

ALTER SEQUENCE admin.formulaire_id_seq OWNED BY admin.formulaire.id;


--
-- TOC entry 196 (class 1259 OID 24686)
-- Name: loisirs; Type: TABLE; Schema: admin; Owner: postgres
--

CREATE TABLE admin.loisirs (
    loisirs_titre character varying(200),
    loisirs_texte character varying(500),
    id integer NOT NULL
);


ALTER TABLE admin.loisirs OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 33021)
-- Name: loisirs_id_seq; Type: SEQUENCE; Schema: admin; Owner: postgres
--

CREATE SEQUENCE admin.loisirs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.loisirs_id_seq OWNER TO postgres;

--
-- TOC entry 2906 (class 0 OID 0)
-- Dependencies: 210
-- Name: loisirs_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: postgres
--

ALTER SEQUENCE admin.loisirs_id_seq OWNED BY admin.loisirs.id;


--
-- TOC entry 199 (class 1259 OID 24701)
-- Name: projets; Type: TABLE; Schema: admin; Owner: postgres
--

CREATE TABLE admin.projets (
    projets_titre character varying(200),
    projets_texte character varying(1000),
    id integer NOT NULL,
    projets_date_debut date,
    projets_date_fin date
);


ALTER TABLE admin.projets OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 33030)
-- Name: projets_id_seq; Type: SEQUENCE; Schema: admin; Owner: postgres
--

CREATE SEQUENCE admin.projets_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.projets_id_seq OWNER TO postgres;

--
-- TOC entry 2907 (class 0 OID 0)
-- Dependencies: 211
-- Name: projets_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: postgres
--

ALTER SEQUENCE admin.projets_id_seq OWNED BY admin.projets.id;


--
-- TOC entry 2740 (class 2604 OID 32966)
-- Name: accueil id; Type: DEFAULT; Schema: admin; Owner: postgres
--

ALTER TABLE ONLY admin.accueil ALTER COLUMN id SET DEFAULT nextval('admin.accueil_id_seq'::regclass);


--
-- TOC entry 2739 (class 2604 OID 32975)
-- Name: contact id; Type: DEFAULT; Schema: admin; Owner: postgres
--

ALTER TABLE ONLY admin.contact ALTER COLUMN id SET DEFAULT nextval('admin.contact_id_seq'::regclass);


--
-- TOC entry 2736 (class 2604 OID 32987)
-- Name: diplomes id; Type: DEFAULT; Schema: admin; Owner: postgres
--

ALTER TABLE ONLY admin.diplomes ALTER COLUMN id SET DEFAULT nextval('admin.diplomes_id_seq'::regclass);


--
-- TOC entry 2738 (class 2604 OID 32999)
-- Name: experiences id; Type: DEFAULT; Schema: admin; Owner: postgres
--

ALTER TABLE ONLY admin.experiences ALTER COLUMN id SET DEFAULT nextval('admin.experiences_id_seq'::regclass);


--
-- TOC entry 2735 (class 2604 OID 33011)
-- Name: formations id; Type: DEFAULT; Schema: admin; Owner: postgres
--

ALTER TABLE ONLY admin.formations ALTER COLUMN id SET DEFAULT nextval('admin.formations_id_seq'::regclass);


--
-- TOC entry 2741 (class 2604 OID 32949)
-- Name: formulaire id; Type: DEFAULT; Schema: admin; Owner: postgres
--

ALTER TABLE ONLY admin.formulaire ALTER COLUMN id SET DEFAULT nextval('admin.formulaire_id_seq'::regclass);


--
-- TOC entry 2734 (class 2604 OID 33023)
-- Name: loisirs id; Type: DEFAULT; Schema: admin; Owner: postgres
--

ALTER TABLE ONLY admin.loisirs ALTER COLUMN id SET DEFAULT nextval('admin.loisirs_id_seq'::regclass);


--
-- TOC entry 2737 (class 2604 OID 33032)
-- Name: projets id; Type: DEFAULT; Schema: admin; Owner: postgres
--

ALTER TABLE ONLY admin.projets ALTER COLUMN id SET DEFAULT nextval('admin.projets_id_seq'::regclass);


--
-- TOC entry 2885 (class 0 OID 24743)
-- Dependencies: 202
-- Data for Name: accueil; Type: TABLE DATA; Schema: admin; Owner: postgres
--

COPY admin.accueil (accueil_titre, id) FROM stdin;
Capucine Bechemin	1
\.


--
-- TOC entry 2884 (class 0 OID 24707)
-- Dependencies: 201
-- Data for Name: contact; Type: TABLE DATA; Schema: admin; Owner: postgres
--

COPY admin.contact (contact_fb, contact_insta, id) FROM stdin;
https://www.facebook.com/capucine.bechemin	https://www.instagram.com/capucine.bcn/	1
\.


--
-- TOC entry 2881 (class 0 OID 24695)
-- Dependencies: 198
-- Data for Name: diplomes; Type: TABLE DATA; Schema: admin; Owner: postgres
--

COPY admin.diplomes (diplomes_photo, diplomes_texte, id) FROM stdin;
diplomes/first.jpg	Diplome certifiant un niveau B2 en Anglais	2
diplomes/psc.jpg	Brevet de secourisme niveau 1, être capable de donner les premiers soins	3
diplomes/bac.jpg	Baccalauréat Scientifique	4
diplomes/permisb.jpg	Permis de conduire	5
diplomes/bafa.jpg	Brevet d Aptitude aux Fonctions d Animateurs	6
\.


--
-- TOC entry 2883 (class 0 OID 24704)
-- Dependencies: 200
-- Data for Name: experiences; Type: TABLE DATA; Schema: admin; Owner: postgres
--

COPY admin.experiences (experiences_texte, id, experiences_entreprise, experiences_date_debut, experiences_date_fin) FROM stdin;
Baby-sitting régulier sur l année 2019/2020 de deux filles de 4 et 8 ans avec l agence Kinougarde.	3	Kinougarde	2019-09-01	2020-07-31
Prise en charge d enfants en colonies en tant qu animatrice.	2	Mairie de saint Herblain et UCPA	2019-07-08	2019-07-26
Stage Informatique	4	\N	2019-06-21	2019-07-05
Stage Cabinet Architechture	5	\N	2016-06-17	2016-06-30
\.


--
-- TOC entry 2880 (class 0 OID 24692)
-- Dependencies: 197
-- Data for Name: formations; Type: TABLE DATA; Schema: admin; Owner: postgres
--

COPY admin.formations (formations_texte, id) FROM stdin;
Lycée Saint Dominique, Saint-Herblain : Cursus général scientifique Sciences de la Vie et de la Terre, spécialitée mathémathiques, option art plastique	8
Anglais L.V.1. niveau B1 à B2	9
Espagnol L.V.2. niveau A2	10
Stage formation générale B.A.F.A.	11
Ecole Ynov ingesup, Nantes. Première année d informatique	12
\.


--
-- TOC entry 2887 (class 0 OID 32946)
-- Dependencies: 204
-- Data for Name: formulaire; Type: TABLE DATA; Schema: admin; Owner: postgres
--

COPY admin.formulaire (id, formulaire_nom, formulaire_mail, formulaire_message) FROM stdin;
1	dvsvdss	csierhogie@firger	vdsvdsvdsvds
2	L'anniversaire de Seb	csierhogie@firger	dnDHFD rgrG fgdgdg 
\.


--
-- TOC entry 2879 (class 0 OID 24686)
-- Dependencies: 196
-- Data for Name: loisirs; Type: TABLE DATA; Schema: admin; Owner: postgres
--

COPY admin.loisirs (loisirs_titre, loisirs_texte, id) FROM stdin;
Bricolage	Aime à la fois construire des petits objet en carton et des meubles en bois	8
Voyage	Aime découvrir de nouvelles cultures, a eu la chance de parcourir 10 pays du monde	9
Peinture et Dessin	Aime dessiner et peindre des personnages de dessins animés, paysage et portrait	7
Musique	Pratique du chant depuis 5 ans en école de musique. Formation d un groupe depuis 4 ans (Feeling Red). Apprentissage de la guitare folk en autodidacte puis cours de guitare électrique durant l année 2017. Apprentissage que différents instruments en autodidacte ; batterie, ukulélé, piano, et plus récemment violon.	6
\.


--
-- TOC entry 2882 (class 0 OID 24701)
-- Dependencies: 199
-- Data for Name: projets; Type: TABLE DATA; Schema: admin; Owner: postgres
--

COPY admin.projets (projets_titre, projets_texte, id, projets_date_debut, projets_date_fin) FROM stdin;
Travaux Pratiques Encadrés (TPE)	En première S, les élèves passe une épreuve qui consiste à réaliser un projet répondant à certains critères technique presque de manière autonome\tNotre sujet était le Transhumanisme, por cela nous avons fait de multiples recherches autant sur la philosophie, le fonctionnement du corps que sur la technicité des prothèses. Nous avons alors pu rendre un site web sous wordpress ainsi qu une main qui attrappe des objets grâce à l impression 3D et Arduino	1	2016-09-20	2017-06-05
Projet Y-Days	L école Ynov nous donne la possibilité de travailler en groupe tout au long de l année sur un projet que l on choisi. Nous sommes évaluer ensuite sur notre façon de gérer un projet et bien sur sur la qualité de la réalisation. Notre groupe s est lancé dans la conception d un site d annecdote à la manière de se coucher moins bête. Nous le voulons cepandant plus dynamique, accessible et jeune. Nous avons travaillé avec des troisième année qui nous ont aiguillés. Nous avons pu aborder concrètement php, sql, html, css, js et l utilisation d un GIT.	2	2018-09-20	2019-06-05
Groupe Feeling Red	Groupe de musique 	7	2016-09-01	2018-12-12
\.


--
-- TOC entry 2908 (class 0 OID 0)
-- Dependencies: 205
-- Name: accueil_id_seq; Type: SEQUENCE SET; Schema: admin; Owner: postgres
--

SELECT pg_catalog.setval('admin.accueil_id_seq', 2, true);


--
-- TOC entry 2909 (class 0 OID 0)
-- Dependencies: 206
-- Name: contact_id_seq; Type: SEQUENCE SET; Schema: admin; Owner: postgres
--

SELECT pg_catalog.setval('admin.contact_id_seq', 2, true);


--
-- TOC entry 2910 (class 0 OID 0)
-- Dependencies: 207
-- Name: diplomes_id_seq; Type: SEQUENCE SET; Schema: admin; Owner: postgres
--

SELECT pg_catalog.setval('admin.diplomes_id_seq', 8, true);


--
-- TOC entry 2911 (class 0 OID 0)
-- Dependencies: 208
-- Name: experiences_id_seq; Type: SEQUENCE SET; Schema: admin; Owner: postgres
--

SELECT pg_catalog.setval('admin.experiences_id_seq', 8, true);


--
-- TOC entry 2912 (class 0 OID 0)
-- Dependencies: 209
-- Name: formations_id_seq; Type: SEQUENCE SET; Schema: admin; Owner: postgres
--

SELECT pg_catalog.setval('admin.formations_id_seq', 13, true);


--
-- TOC entry 2913 (class 0 OID 0)
-- Dependencies: 203
-- Name: formulaire_id_seq; Type: SEQUENCE SET; Schema: admin; Owner: postgres
--

SELECT pg_catalog.setval('admin.formulaire_id_seq', 2, true);


--
-- TOC entry 2914 (class 0 OID 0)
-- Dependencies: 210
-- Name: loisirs_id_seq; Type: SEQUENCE SET; Schema: admin; Owner: postgres
--

SELECT pg_catalog.setval('admin.loisirs_id_seq', 12, true);


--
-- TOC entry 2915 (class 0 OID 0)
-- Dependencies: 211
-- Name: projets_id_seq; Type: SEQUENCE SET; Schema: admin; Owner: postgres
--

SELECT pg_catalog.setval('admin.projets_id_seq', 7, true);


--
-- TOC entry 2755 (class 2606 OID 32968)
-- Name: accueil accueil_pkey; Type: CONSTRAINT; Schema: admin; Owner: postgres
--

ALTER TABLE ONLY admin.accueil
    ADD CONSTRAINT accueil_pkey PRIMARY KEY (id);


--
-- TOC entry 2753 (class 2606 OID 32977)
-- Name: contact contact_pkey; Type: CONSTRAINT; Schema: admin; Owner: postgres
--

ALTER TABLE ONLY admin.contact
    ADD CONSTRAINT contact_pkey PRIMARY KEY (id);


--
-- TOC entry 2747 (class 2606 OID 32989)
-- Name: diplomes diplomes_pkey; Type: CONSTRAINT; Schema: admin; Owner: postgres
--

ALTER TABLE ONLY admin.diplomes
    ADD CONSTRAINT diplomes_pkey PRIMARY KEY (id);


--
-- TOC entry 2751 (class 2606 OID 33001)
-- Name: experiences experiences_pkey; Type: CONSTRAINT; Schema: admin; Owner: postgres
--

ALTER TABLE ONLY admin.experiences
    ADD CONSTRAINT experiences_pkey PRIMARY KEY (id);


--
-- TOC entry 2745 (class 2606 OID 33013)
-- Name: formations formations_pkey; Type: CONSTRAINT; Schema: admin; Owner: postgres
--

ALTER TABLE ONLY admin.formations
    ADD CONSTRAINT formations_pkey PRIMARY KEY (id);


--
-- TOC entry 2757 (class 2606 OID 32954)
-- Name: formulaire formulaire_pkey; Type: CONSTRAINT; Schema: admin; Owner: postgres
--

ALTER TABLE ONLY admin.formulaire
    ADD CONSTRAINT formulaire_pkey PRIMARY KEY (id);


--
-- TOC entry 2743 (class 2606 OID 33025)
-- Name: loisirs loisirs_pkey; Type: CONSTRAINT; Schema: admin; Owner: postgres
--

ALTER TABLE ONLY admin.loisirs
    ADD CONSTRAINT loisirs_pkey PRIMARY KEY (id);


--
-- TOC entry 2749 (class 2606 OID 33034)
-- Name: projets projets_pkey; Type: CONSTRAINT; Schema: admin; Owner: postgres
--

ALTER TABLE ONLY admin.projets
    ADD CONSTRAINT projets_pkey PRIMARY KEY (id);


-- Completed on 2019-06-06 11:03:56

--
-- PostgreSQL database dump complete
--

