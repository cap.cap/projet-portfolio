
<?php

    if (isset($_POST['experiences_texte']) && !empty($_POST['experiences_texte']) && isset($_POST['experiences_entreprise']) && !empty($_POST['experiences_entreprise'])&& isset($_POST['experiences_date_debut']) && !empty($_POST['experiences_date_debut'])&& isset($_POST['experiences_date_fin']) && !empty($_POST['experiences_date_fin'])){

        $experiencesTexte = $_POST['experiences_texte'];
        $experiencesEntreprise = $_POST['experiences_entreprise'];
        $experiencesDateD = $_POST['experiences_date_debut'];
        $experiencesDateF = $_POST['experiences_date_fin'];
        $sth = $dbh->prepare("INSERT INTO admin.experiences(experiences_texte, experiences_entreprise, experiences_date_debut, experiences_date_fin) VALUES('".$experiencesTexte."','".$experiencesEntreprise."','".$experiencesDateD."', '".$experiencesDateF."')");
        $sth->execute();
    }
?>

<h2>Experiences Professionnelles</h2>

<form id="AexperiencesPro" method="post" action="administration.php?page=experiences" enctype="multipart/form-data">

    <div id="contenu">
        <div>
            <h3>Ajouter une experience</h3>
            <input type="text" name="experiences_entreprise" placeholder="Entreprise">
            <label>Date de début : </label>
            <input type="date" name="experiences_date_debut">
            <label>Date de fin : </label>
            <input type="date" name="experiences_date_fin">
            <textarea name="experiences_texte"></textarea>
            <input type="submit" name="valider" value="valider">
        </div>

        <div>
            <h3>Supprimer une experience</h3>
            <select name="experiences_suppr">
            <?php
            while ($row = $adminExperiences->fetch(PDO::FETCH_ASSOC)):
                ?>
                <option value="<?= $row['id']?>"><?= $row['experiences_texte']?></option>
            <?php
            endwhile;
            ?>
            </select>
            <button type="submit" id="supprimer" name="supprimer">Supprimer</button>
            <?php
            if (isset($_POST['supprimer'])) {
                $delete = $dbh->prepare("DELETE FROM admin.experiences WHERE id = :id");
                $delete->bindValue('id', $_POST['experiences_suppr']);
                $delete->execute();
            }
            ?>
        </div>
    </div>

</form>