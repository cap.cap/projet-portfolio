<?php

    if (isset($_POST['valider'])&& isset($_POST['projets_texte']) && !empty($_POST['projets_texte']) && isset($_POST['projets_titre']) && !empty($_POST['projets_titre'])&& isset($_POST['projets_date_debut']) && !empty($_POST['projets_date_debut'])&& isset($_POST['projets_date_fin']) && !empty($_POST['projets_date_fin'])){
        $projetsTitre = $_POST['projets_titre'];
        $projetsTexte = $_POST['projets_texte'];
        $projetsDateD = $_POST['projets_date_debut'];
        $projetsDateF = $_POST['projets_date_fin'];

        $sth = $dbh->prepare("INSERT INTO admin.projets(projets_titre, projets_texte, projets_date_debut, projets_date_fin) VALUES('".$projetsTitre."','".$projetsTexte."','".$projetsDateD."','".$projetsDateF."')");
        $sth->execute();
    }
?>

<h2>Projets</h2>
<form id="Aprojets" method="post" action="administration.php?page=projets" enctype="multipart/form-data">

        <div id="contenu">
            <div>
                <h3>Ajouter un projet</h3>
                <input type="text" name="projets_titre" placeholder="Titre">
                <br>
                <label>Date de début</label>
                <input type="date" name="projets_date_debut">
                <br>
                <label>Date de fin</label>
                <input type="date" name="projets_date_fin">
                <br>
                <textarea placeholder="Descriptif" name="projets_texte"></textarea>
                <input type="submit" name="valider" value="valider">
            </div>

            <div>
                <h3>Supprimer un projet</h3>
                <select name="projets_suppr">
                    <?php
                    while ($row = $adminProjets->fetch(PDO::FETCH_ASSOC)):
                        ?>
                        <option value="<?= $row['id']?>"><?= $row['projets_titre']?></option>
                    <?php
                        endwhile;
                    ?>
                </select>
                <button type="submit" id="supprimer" name="supprimer">Supprimer</button>
                <?php
                if (isset($_POST['supprimer'])) {
                    $delete = $dbh->prepare("DELETE FROM admin.projets WHERE id = :id");
                    $delete->bindValue('id', $_POST['projets_suppr']);
                    $delete->execute();
                }
                ?>
            </div>
        </div>

</form>
