<?php

    if (isset($_POST['valider']) && isset($_POST['loisirs_texte']) && !empty($_POST['loisirs_texte']) && isset($_POST['loisirs_titre']) && !empty($_POST['loisirs_titre'])){
        $loisirsTitre = $_POST['loisirs_titre'];
        $loisirsTexte = $_POST['loisirs_texte'];

        $sth = $dbh->prepare("INSERT INTO admin.loisirs(loisirs_titre, loisirs_texte) VALUES('".$loisirsTitre."','".$loisirsTexte."')");
        $sth->execute();
    }
?>


<h2>Loisirs</h2>
<form id="Aloisirs" method="post" action="administration.php?page=loisirs" enctype="multipart/form-data">

    <div id="contenu">
        <div>
            <h3>Ajouter un loisirs</h3>
            <input type="text" name="loisirs_titre" placeholder="Titre">
            <br>
            <textarea name="loisirs_texte" value="Description" placeholder="Description"></textarea>
            <input type="submit" name="valider" value="valider">
        </div>

        <div>
            <h3>Supprimer un loisirs</h3>

            <select name="loisirs_suppr">
                <?php
                    while ($row = $adminLoisirs->fetch(PDO::FETCH_ASSOC)):
                ?>
                    <option value="<?= $row['id']?>"><?= $row['loisirs_titre']?></option>
                <?php
                    endwhile;
                ?>
            </select>
            <button type="submit" id="supprimer" name="supprimer">Supprimer</button>
            <?php
            if (isset($_POST['supprimer'])) {
                $delete = $dbh->prepare("DELETE FROM admin.loisirs WHERE id = :id");
                $delete->bindValue('id', $_POST['loisirs_suppr']);
                $delete->execute();
            }
            ?>

        </div>
    </div>

</form>