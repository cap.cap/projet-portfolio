<nav class="nav-bar">
    <a class="nav-link <?php if($page =='portfolio'){echo "active";};?>" href="../portfolio.php">PORTFOLIO</a>
    <div id="colonne2">
        <a class="nav-link <?php if($page =='accueil'){echo "active";};?>" href="?page=accueil">Accueil</a>
        <a class="nav-link <?php if($page =='formations'){echo "active";};?>" href="?page=formations">Formations</a>
        <a class="nav-link <?= ($page =='projets')?"active":''?>" href="?page=projets">Projets</a>
        <a class="nav-link <?= ($page =='diplomes')?"active":''?>" href="?page=diplomes">Diplomes</a>
    </div>
    <div id="colonne2">
        <a class="nav-link <?= ($page =='experiences')?"active":''?>" href="?page=experiences">Experiences Professionnelles</a>
        <a class="nav-link <?= ($page =='loisirs')?"active":''?>" href="?page=loisirs">Loisirs</a>
        <a class="nav-link <?= ($page =='contact')?"active":''?>" href="?page=contact">Contact</a>
        <a class="nav-link <?= ($page =='resultat_formulaire')?"active":''?>" href="?page=resultat_formulaire">Resultat du Formulaire</a>
    </div>
</nav>