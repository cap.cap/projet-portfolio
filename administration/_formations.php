
<?php

    if (isset($_POST['valider']) && isset($_POST['formations_texte']) && !empty($_POST['formations_texte'])){
        $formationsTexte = $_POST['formations_texte'];
        $sth = $dbh->prepare("INSERT INTO admin.formations(formations_texte) VALUES('".$formationsTexte."')");
        $sth->execute();
    }


?>



<h2>Formations</h2>
<form id="Aformations" method="post" action="administration.php?page=formations" enctype="multipart/form-data">

    <div id="contenu">
        <div>
            <h3>Ajouter une formation</h3>
            <textarea name="formations_texte"></textarea>
            <input type="submit" name="valider" value="valider">

        </div>
        <div>
            <h3>Supprimer une formation</h3>

            <select name="formation_suppr">
            <?php
                while ($row = $adminFormations->fetch(PDO::FETCH_ASSOC)):
            ?>
                <option value="<?= $row['id']?>"><?= $row['formations_texte']?></option>
            <?php
              endwhile;
            ?>
            </select>
            <button type="submit" id="supprimer" name="supprimer">Supprimer</button>
            <?php
                if (isset($_POST['supprimer'])) {
                    $delete = $dbh->prepare("DELETE FROM admin.formations WHERE id = :id");
                    $delete->bindValue('id', $_POST['formation_suppr']);
                    $delete->execute();
                }
            ?>

        </div>

    </div>

</form>