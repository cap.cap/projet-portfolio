<?php
    session_start();

    if(isset($_GET['page'])){
        $page= $_GET['page'];
    }else{
        $page = '_menu.php';
    }


    $dsn = 'pgsql:host=127.0.0.1;dbname=portfolio';
    $user = 'postgres';
    $password = 'PostGre1379';

    try {
        $dbh = new PDO($dsn, $user, $password);
    } catch (PDOException $e) {
        echo 'Connexion échouée : ' . $e->getMessage();
    }

    $adminAccueil = $dbh->query("SELECT * FROM admin.accueil");
    $adminFormations = $dbh->query("SELECT * FROM admin.formations");
    $adminProjets = $dbh->query("SELECT * FROM admin.projets");
    $adminDiplomes = $dbh->query("SELECT * FROM admin.diplomes");
    $adminExperiences = $dbh->query("SELECT * FROM admin.experiences");
    $adminLoisirs = $dbh->query("SELECT * FROM admin.loisirs");
    $adminContact = $dbh->query("SELECT * FROM admin.contact");
    $adminFormulaire = $dbh->query("SELECT * FROM admin.formulaire");
?>


<!DOCTYPE html>
<html>
<head>
    <link rel="icon" type="img/jpg" href="../moi.png">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="stylesheet" type="text/css" href="administration.css">
	<link rel="shortcut icon" type="image/png" href="moi.png">
	<title>C.B. Administration</title>
</head>
<header>
    <h1>Administration Portfolio</h1>
    <?php
        require_once '_menu.php';
    ?>
</header>

<body>

<?php

    switch ($page) {
        case 'accueil':
            require_once '_accueil.php';
            break;

        case 'formations':
            require_once '_formations.php';
            break;

        case 'diplomes':
            require_once '_diplomes.php';
            break;

        case 'projets':
            require_once '_projets.php';
            break;

        case 'experiences':
            require_once '_experiences.php';
            break;

        case 'loisirs':
            require_once '_loisirs.php';
            break;

        case 'contact':
            require_once '_contact.php';
            break;

        case 'resultat_formulaire':
            require_once '_resultat_formulaire.php';
            break;
    }
?>

</body>
</html>